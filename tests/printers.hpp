/*
 * Copyright (C) 2018 Michał Góral.
 * 
 * This file is part of Yaxo
 * 
 * Yaxo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Yaxo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Yaxo. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PRINTERS_HPP_
#define PRINTERS_HPP_

#include "rcparser.hpp"

#include <QString>
#include <QMimeType>

#include <ostream>

inline std::ostream& operator<< (std::ostream& os, const QString &qs)
{
	os << qs.toStdString();
	return os;
}

inline std::ostream& operator<< (std::ostream& os, const QMimeType &mime)
{
	os << mime.name();
	return os;
}

#endif  // PRINTERS_HPP_
