/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright (C) 2023 Michał Góral.
 */

#include "do_main.hpp"
#include "executor.hpp"
#include "str.hpp"
#include "arginfo.hpp"

#include <stdlib.h>

#include <string>
#include <algorithm>
#include <QCoreApplication>
#include <QTemporaryFile>
#include <QTextStream>

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_vector.hpp>
#include <catch2/generators/catch_generators.hpp>

class MockExecutor : public yaxo::Executor {
public:
    bool exec(const QString& cmd, const std::vector<yaxo::ArgInfo>& callargs) const override final {
        calls.push_back(cmd);
        args.push_back(callargs);
        return return_value;
    }

    std::vector<std::vector<QString>> argargs() {
        std::vector<std::vector<QString>> ret;
        for (const auto& callargs : args)
        {
            std::vector<QString> in;
            std::transform(callargs.begin(), callargs.end(), std::back_inserter(in),
                [](const auto& ca){ return ca.arg; });
            ret.push_back(in);
        }

        return ret;
    }

    mutable std::vector<QString> calls;
    mutable std::vector<std::vector<yaxo::ArgInfo>> args;
    bool return_value = true;
};

class Sut {
public:
    Sut& arg(const QString &arg) {
        args_.push_back(arg.toStdString());
        ++argc_;
        return *this;
    }

    int run(MockExecutor &ex) {
        auto app = make_app();
        return do_main(app, ex);
    }

private:
    QCoreApplication make_app() {
        argv_.clear();
        for (auto &arg : args_) {
            argv_.push_back(static_cast<char*>(arg.data()));
        }

        return QCoreApplication(argc_, argv_.data());
    }

    std::vector<std::string> args_ = { "yaxo" };
    std::vector<char*> argv_;
    int argc_ = 1;

};

std::vector<char*> to_argv(const std::vector<QString> &args)
{
    std::vector<char*> argv;
    for (const auto &arg : args) {
        argv.push_back(static_cast<char*>(arg.toStdString().data()));
    }
    return argv;
}

TEST_CASE("End-to-end tests")
{
    QTemporaryFile yaxorc("XXXXXX.yaxorc");
    yaxorc.open();

    QTemporaryFile text_file("XXXXXX.txt");
    text_file.open();

    QTemporaryFile bin_file("XXXXXX.bin");
    bin_file.open();

    unsetenv("BROWSER");
    unsetenv("VISUAL");
    unsetenv("EDITOR");

    MockExecutor ex;
    Sut sut;

    SECTION("find match") {
        QTextStream ts(&yaxorc);
        ts << "ext foo: cmd -- foo\n";
        ts << "ext txt: cmd -- bar\n";
        ts << "ext txt: cmd -- baz\n";
        ts.flush();
        yaxorc.seek(0);

        sut.arg("--config").arg(yaxorc.fileName());
        sut.arg(text_file.fileName());
        sut.arg("--verbose");

        CHECK(sut.run(ex) == EXIT_SUCCESS);
        REQUIRE(ex.calls.size() == 1);
        REQUIRE(ex.args.size() == 1);
        CHECK_THAT(ex.calls, Catch::Matchers::Equals(std::vector<QString>{ "cmd -- bar" }));
        CHECK_THAT(ex.argargs()[0], Catch::Matchers::Equals(std::vector<QString>{
            text_file.fileName()
        }));
    }

    SECTION("don't find match") {
        QTextStream ts(&yaxorc);
        ts << "ext foo: cmd -- foo\n";
        ts << "ext bar: cmd -- bar\n";
        ts << "ext baz: cmd -- baz\n";
        ts.flush();
        yaxorc.seek(0);

        sut.arg("--config").arg(yaxorc.fileName());
        sut.arg(bin_file.fileName());
        sut.arg("--verbose");

        CHECK(sut.run(ex) == EXIT_FAILURE);
        REQUIRE(ex.calls.size() == 0);
        REQUIRE(ex.args.size() == 0);
    }

    SECTION("many files") {
        QTextStream ts(&yaxorc);
        ts << "ext txt: cmd -- foo\n";
        ts << "ext bar: cmd -- bar\n";
        ts << "ext baz: cmd -- baz\n";
        ts.flush();
        yaxorc.seek(0);

        sut.arg("--config").arg(yaxorc.fileName());
        sut.arg(text_file.fileName());
        sut.arg(text_file.fileName());
        sut.arg(text_file.fileName());
        sut.arg("--verbose");

        CHECK(sut.run(ex) == EXIT_SUCCESS);
        REQUIRE(ex.calls.size() == 1);
        REQUIRE(ex.args.size() == 1);
        CHECK_THAT(ex.calls, Catch::Matchers::Equals(std::vector<QString>{ "cmd -- foo" }));
        CHECK_THAT(ex.argargs()[0], Catch::Matchers::Equals(std::vector<QString>{
            text_file.fileName(),
            text_file.fileName(),
            text_file.fileName(),
        }));
    }

    SECTION("complex conditions") {
        QTextStream ts(&yaxorc);
        ts << "!ext txt, !ext bin: cmd -- 1\n";
        ts << "mime text/.*, if terminal, mime image/.*: cmd -- 2\n";
        ts << "mime text/.*, !mime image/.*, !ext txt: cmd -- 3\n";
        ts << "mime text/.*, !mime image/.*, ext txt: cmd -- 4\n";
        ts << "!ext txt, ext bin: cmd -- 5\n";
        ts.flush();
        yaxorc.seek(0);

        sut.arg("--config").arg(yaxorc.fileName());
        sut.arg(bin_file.fileName());
        sut.arg("--verbose");

        CHECK(sut.run(ex) == EXIT_SUCCESS);
        REQUIRE(ex.calls.size() == 1);
        REQUIRE(ex.args.size() == 1);
        CHECK_THAT(ex.calls, Catch::Matchers::Equals(std::vector<QString>{ "cmd -- 5" }));
        CHECK_THAT(ex.argargs()[0], Catch::Matchers::Equals(std::vector<QString>{
            bin_file.fileName(),
        }));
    }

    SECTION("url with default browser") {
        sut.arg("--config").arg(yaxorc.fileName());
        sut.arg("https://example.com");
        sut.arg("--verbose");

        setenv("BROWSER", "mybrowser", 1);

        CHECK(sut.run(ex) == EXIT_SUCCESS);
        REQUIRE(ex.calls.size() == 1);
        REQUIRE(ex.args.size() == 1);
        CHECK_THAT(ex.calls, Catch::Matchers::Equals(std::vector<QString>{ "$BROWSER \"$@\"" }));
        CHECK_THAT(ex.argargs()[0], Catch::Matchers::Equals(std::vector<QString>{
            "https://example.com",
        }));
    }

    SECTION("url with no $BROWSER set") {
        sut.arg("--config").arg(yaxorc.fileName());
        sut.arg("https://example.com");
        sut.arg("--verbose");

        CHECK(sut.run(ex) == EXIT_FAILURE);
        REQUIRE(ex.calls.size() == 0);
        REQUIRE(ex.args.size() == 0);
    }

    SECTION("default visual editor") {
        sut.arg("--config").arg(yaxorc.fileName());
        sut.arg(text_file.fileName());
        sut.arg("--verbose");

        setenv("VISUAL", "foo", 1);
        setenv("EDITOR", "bar", 1);

        CHECK(sut.run(ex) == EXIT_SUCCESS);
        REQUIRE(ex.calls.size() == 1);
        REQUIRE(ex.args.size() == 1);
        CHECK_THAT(ex.calls, Catch::Matchers::Equals(std::vector<QString>{ "$VISUAL \"$@\"" }));
        CHECK_THAT(ex.argargs()[0], Catch::Matchers::Equals(std::vector<QString>{
            text_file.fileName()
        }));
    }

    SECTION("default editor") {
        sut.arg("--config").arg(yaxorc.fileName());
        sut.arg(text_file.fileName());
        sut.arg("--verbose");

        setenv("EDITOR", "foo", 1);

        CHECK(sut.run(ex) == EXIT_SUCCESS);
        REQUIRE(ex.calls.size() == 1);
        REQUIRE(ex.args.size() == 1);
        CHECK_THAT(ex.calls, Catch::Matchers::Equals(std::vector<QString>{ "$EDITOR \"$@\"" }));
        CHECK_THAT(ex.argargs()[0], Catch::Matchers::Equals(std::vector<QString>{
            text_file.fileName()
        }));
    }

    SECTION("no default match") {
        sut.arg("--config").arg(yaxorc.fileName());
        sut.arg(text_file.fileName());
        sut.arg("--verbose");

        CHECK(sut.run(ex) == EXIT_FAILURE);
        REQUIRE(ex.calls.size() == 0);
        REQUIRE(ex.args.size() == 0);
    }

    SECTION("conditions") {
        auto rule = GENERATE(as<QString>{},
            "path .*\\.bin: cmd",
            "ext bin: cmd",
            "mime application/octet-stream: cmd",
            "scheme file: cmd",
            "env FOO: cmd",
            "if X: cmd",
            "if Wayland: cmd",
            "if graphical: cmd",
            "if exists: cmd"
        );

        setenv("FOO", "yes", 1);
        setenv("DISPLAY", ":0", 1);
        setenv("WAYLAND_DISPLAY", "wayland-1", 1);

        QTextStream ts(&yaxorc);
        ts << rule << "\n";
        ts.flush();
        yaxorc.seek(0);

        sut.arg("--config").arg(yaxorc.fileName());
        sut.arg(bin_file.fileName());
        sut.arg("--verbose");

        CHECK(sut.run(ex) == EXIT_SUCCESS);
        REQUIRE(ex.calls.size() == 1);
        REQUIRE(ex.args.size() == 1);
        CHECK_THAT(ex.calls, Catch::Matchers::Equals(std::vector<QString>{ "cmd" }));
        CHECK_THAT(ex.argargs()[0], Catch::Matchers::Equals(std::vector<QString>{
            bin_file.fileName(),
        }));
    }

    SECTION("condition negation") {
        auto rule = GENERATE(as<QString>{},
            "!path .*\\.txt: cmd",
            "!ext txt: cmd",
            "!mime foo/bar: cmd",
            "!scheme https: cmd",
            "!env VISUAL: cmd",
            "!if X: cmd",
            "!if Wayland: cmd",
            "!if graphical: cmd"
        );

        unsetenv("DISPLAY");
        unsetenv("WAYLAND_DISPLAY");

        QTextStream ts(&yaxorc);
        ts << rule << "\n";
        ts.flush();
        yaxorc.seek(0);

        sut.arg("--config").arg(yaxorc.fileName());
        sut.arg(bin_file.fileName());
        sut.arg("--verbose");

        CHECK(sut.run(ex) == EXIT_SUCCESS);
        REQUIRE(ex.calls.size() == 1);
        REQUIRE(ex.args.size() == 1);
        CHECK_THAT(ex.calls, Catch::Matchers::Equals(std::vector<QString>{ "cmd" }));
        CHECK_THAT(ex.argargs()[0], Catch::Matchers::Equals(std::vector<QString>{
            bin_file.fileName(),
        }));
    }
}
