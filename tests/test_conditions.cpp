/*
 * Copyright (C) 2018 Michał Góral.
 * 
 * This file is part of Yaxo
 * 
 * Yaxo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Yaxo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Yaxo. If not, see <http://www.gnu.org/licenses/>.
 */

#include "conditions.hpp"
#include "arginfo.hpp"

#include <QTemporaryFile>

#include <catch2/catch_test_macros.hpp>

class Always : public yaxo::Condition
{
public:
    explicit Always(bool ret) : ret_(ret) {}

protected:
    bool check(const yaxo::ArgInfo&) const override { return ret_; }
    QString describeCheck(const yaxo::ArgInfo&) const override { return QString(); };

private:
    bool ret_;
};


TEST_CASE("extension matching")
{
    SECTION("existing file") {
        QTemporaryFile tmp("XXXXXX.txt");
        tmp.open();
        auto file = yaxo::ArgInfo(tmp.fileName());

        CHECK(yaxo::Extension{".*"}(file));
        CHECK(yaxo::Extension{"txt"}(file));
        CHECK(yaxo::Extension{".xt"}(file));

        CHECK_FALSE(yaxo::Extension{"*"}(file));
        CHECK_FALSE(yaxo::Extension{".txt"}(file));
        CHECK_FALSE(yaxo::Extension{"\\.txt"}(file));
        CHECK_FALSE(yaxo::Extension{"xt"}(file));
    }

    SECTION("unexisting file") {
        QString name;

        {
            QTemporaryFile tmp("XXXXXX.txt");
            tmp.open();
            name = tmp.fileName();
        }

        auto file = yaxo::ArgInfo(name);

        CHECK(yaxo::Extension{".*"}(file));
        CHECK(yaxo::Extension{"txt"}(file));
        CHECK(yaxo::Extension{".xt"}(file));

        CHECK_FALSE(yaxo::Extension{"*"}(file));
        CHECK_FALSE(yaxo::Extension{".txt"}(file));
        CHECK_FALSE(yaxo::Extension{"\\.txt"}(file));
        CHECK_FALSE(yaxo::Extension{"xt"}(file));
    }

    SECTION("complex extension") {
        QTemporaryFile tmp("XXXXXX.tar.gz");
        tmp.open();
        auto file = yaxo::ArgInfo(tmp.fileName());

        CHECK(yaxo::Extension{".*"}(file));
        CHECK(yaxo::Extension{"tar.gz"}(file));
        CHECK(yaxo::Extension{".*gz"}(file));
        CHECK(yaxo::Extension{".*tar.gz"}(file));

        CHECK_FALSE(yaxo::Extension{".tar.gz"}(file));
        CHECK_FALSE(yaxo::Extension{"\\.tar.gz"}(file));
        CHECK_FALSE(yaxo::Extension{"gz"}(file));
        CHECK_FALSE(yaxo::Extension{".gz"}(file));
        CHECK_FALSE(yaxo::Extension{"\\.gz"}(file));
    }
}

TEST_CASE("mimetype matching")
{
    auto file = yaxo::ArgInfo("abc.txt");
    CAPTURE(file.mime);

    CHECK(yaxo::Mime("text/plain")(file));
    CHECK(yaxo::Mime(".*")(file));
    CHECK(yaxo::Mime("text/.*")(file));

    CHECK_FALSE(yaxo::Mime("text")(file));
    CHECK_FALSE(yaxo::Mime("ext.*")(file));
}

TEST_CASE("Scheme matching")
{
    SECTION("http") {
        auto file = yaxo::ArgInfo("http://example.com");
        file.scheme = "http";

        CAPTURE(file.scheme);

        CHECK(yaxo::Scheme(".*")(file));
        CHECK(yaxo::Scheme("http")(file));
        CHECK(yaxo::Scheme("https?")(file));

        CHECK_FALSE(yaxo::Scheme("ttp")(file));
        CHECK_FALSE(yaxo::Scheme("https")(file));
    }

    SECTION("file") {
        auto file = yaxo::ArgInfo("abc.txt");

        CAPTURE(file.scheme);

        CHECK(yaxo::Scheme(".*")(file));
        CHECK(yaxo::Scheme("file")(file));
        CHECK(yaxo::Scheme("file.*")(file));
    }
}

TEST_CASE("Path matching")
{
    SECTION("http") {
        auto file = yaxo::ArgInfo("http://example.com");

        CAPTURE(file.scheme);

        CHECK(yaxo::Path(".*")(file));
        CHECK(yaxo::Path("http://example.com")(file));
        CHECK(yaxo::Path("http://.*")(file));

        CHECK_FALSE(yaxo::Path("ttp")(file));
        CHECK_FALSE(yaxo::Path("https")(file));
    }

    SECTION("file") {
        auto file = yaxo::ArgInfo("abc.txt");

        CAPTURE(file.scheme);

        CHECK(yaxo::Path(".*")(file));
        CHECK(yaxo::Path(".*\\.txt")(file));
        CHECK(yaxo::Path("abc.*")(file));

        CHECK_FALSE(yaxo::Path("bc.*")(file));
        CHECK_FALSE(yaxo::Path("/usr/.*abc.txt")(file));
    }

    SECTION("file path") {
        auto file = yaxo::ArgInfo("/usr/abc.txt");

        CAPTURE(file.scheme);

        CHECK(yaxo::Path(".*")(file));
        CHECK(yaxo::Path(".*\\.txt")(file));
        CHECK(yaxo::Path("/usr.*")(file));
        CHECK(yaxo::Path(".*abc.txt")(file));

        CHECK_FALSE(yaxo::Path("abc.txt")(file));
    }
}

TEST_CASE("Property matching")
{
    {
        QTemporaryFile tmp("XXXXXX.txt");
        tmp.open();
        auto file = yaxo::ArgInfo(tmp.fileName());

        QString unexistingName;
        {
            QTemporaryFile tmp("XXXXXX.txt");
            tmp.open();
            unexistingName = tmp.fileName();
        }
        auto unexisting = yaxo::ArgInfo(unexistingName);

        auto url = yaxo::ArgInfo("https://example.com");

        QProcessEnvironment env;

        SECTION("exists") {
            CHECK(yaxo::PropCheck("exists", env)(file));
            CHECK_FALSE(yaxo::PropCheck("exists", env)(unexisting));
            CHECK_FALSE(yaxo::PropCheck("exists", env)(url));
        }

        SECTION("no graphical environment") {
            CHECK_FALSE(yaxo::PropCheck("X", env)(file));
            CHECK_FALSE(yaxo::PropCheck("Wayland", env)(file));
            CHECK_FALSE(yaxo::PropCheck("graphical", env)(file));
        }

        SECTION("X") {
            env.insert("DISPLAY", ":1");

            CHECK(yaxo::PropCheck("X", env)(file));
            CHECK_FALSE(yaxo::PropCheck("Wayland", env)(file));
            CHECK(yaxo::PropCheck("graphical", env)(file));
        }

        SECTION("Wayland") {
            env.insert("WAYLAND_DISPLAY", "wayland-1");

            CHECK_FALSE(yaxo::PropCheck("X", env)(file));
            CHECK(yaxo::PropCheck("Wayland", env)(file));
            CHECK(yaxo::PropCheck("graphical", env)(file));
        }
    }
}

TEST_CASE("Env matching")
{
    QProcessEnvironment env;

    QTemporaryFile tmp("XXXXXX.txt");
    tmp.open();
    auto file = yaxo::ArgInfo(tmp.fileName());

    SECTION("variable set to value") {
        env.insert("FOO", "1");
        CHECK(yaxo::EnvCheck("FOO", env)(file));

        env.insert("BAR", "0");
        CHECK(yaxo::EnvCheck("BAR", env)(file));

        env.insert("BAZ", "abc");
        CHECK(yaxo::EnvCheck("BAZ", env)(file));
    }

    SECTION("variable is empty") {
        env.insert("FOO", "");
        CHECK_FALSE(yaxo::EnvCheck("FOO", env)(file));
    }

    SECTION("variable not set") {
        CHECK_FALSE(yaxo::EnvCheck("FOO", env)(file));
    }
}

TEST_CASE("Not matching")
{
    QTemporaryFile tmp("XXXXXX.txt");
    tmp.open();
    auto file = yaxo::ArgInfo(tmp.fileName());

    CHECK_FALSE(yaxo::Not(std::make_unique<Always>(true))(file));
    CHECK(yaxo::Not(std::make_unique<Always>(false))(file));
}
