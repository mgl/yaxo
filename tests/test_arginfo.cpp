/*
 * Copyright (C) 2018 Michał Góral.
 * 
 * This file is part of Yaxo
 * 
 * Yaxo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Yaxo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Yaxo. If not, see <http://www.gnu.org/licenses/>.
 */

#include "arginfo.hpp"
#include "printers.hpp"

#include <QTemporaryFile>

#include <catch2/catch_test_macros.hpp>

TEST_CASE("constructiong")
{
    SECTION("file") {
        QTemporaryFile tmp("XXXXXX.txt");
        tmp.open();

        auto info = yaxo::ArgInfo(tmp.fileName());

        CHECK(info.arg == tmp.fileName());
        CHECK(info.scheme == "file");
        CHECK(info.ext == "txt");
        CHECK(info.exists);

        // we could check if it's text/plain, but it heavily depends on running
        // OS configuration, so instead we'll just check if mimetype is valid.
        CHECK(info.mime.isValid());
    }

    SECTION("unexisting file") {
        QString name;

        {
            QTemporaryFile tmp("XXXXXX.txt");
            tmp.open();
            name = tmp.fileName();
        }

        auto info = yaxo::ArgInfo(name);

        INFO(info.mime);

        CHECK(info.arg == name);
        CHECK(info.scheme == "file");
        CHECK(info.ext == "txt");
        CHECK(info.mime.isValid());
        CHECK(!info.exists);
    }

    SECTION("urls") {
        auto schemes = { "http", "https", "magnet", "irc" };

        for (const auto& sch : schemes) {
            auto arg = QString(sch) + "://example.com";

            INFO(arg);

            auto info = yaxo::ArgInfo(arg);

            CHECK(info.arg == arg);
            CHECK(info.scheme == sch);
            CHECK(info.ext.isEmpty());
            CHECK(info.mime.isValid());
            CHECK(!info.exists);
        }
    }

    SECTION("long extension") {
        auto info = yaxo::ArgInfo("archive.tar.gz");
        CHECK(info.ext == "tar.gz");
    }
}
