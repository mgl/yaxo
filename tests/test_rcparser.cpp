/*
 * Copyright (C) 2018 Michał Góral.
 *
 * This file is part of Yaxo
 *
 * Yaxo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Yaxo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Yaxo. If not, see <http://www.gnu.org/licenses/>.
 */

#include "rcparser.hpp"
#include "printers.hpp"

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

TEST_CASE("RCLine")
{
    auto line = yaxo::RCLine();
    REQUIRE(line.empty());

    SECTION("contains condition") {
        line.conditions.emplace_back();
        REQUIRE_FALSE(line.empty());
    }

    SECTION("contains command") {
        line.command = "foo";
        REQUIRE_FALSE(line.empty());
    }

    SECTION("contains command and condition") {
        line.command = "foo";
        line.conditions.emplace_back();
        REQUIRE_FALSE(line.empty());
    }
}

TEST_CASE("line parsing")
{
    QString input;
    QTextStream stream(&input);
    QProcessEnvironment env;

    SECTION("eof") {
        stream << "";
        auto result = yaxo::nextLine(env, stream);

        CHECK(result.line.empty());
        CHECK(result.verdict == yaxo::Verdict::eof);
    }

    SECTION("empty line") {
        auto tested = { "", "  " };
        for (auto line : tested) {
            stream << line << "\n";
            auto result = yaxo::nextLine(env, stream);

            CAPTURE(line);
            CAPTURE(result.msg.toStdString());

            CHECK(result.line.empty());
            CHECK(result.verdict == yaxo::Verdict::skip);
        }
    }

    SECTION("comment") {
        auto line = GENERATE(as<QString>{}, "#", "    #", "  # foo", "#foo", " #foo");
        stream << line << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(line);
        CAPTURE(result.msg.toStdString());

        CHECK(result.line.empty());
        CHECK(result.verdict == yaxo::Verdict::skip);
    }

    SECTION("incorrect syntax") {
        auto line = GENERATE(as<QString>{},
            "abc",
            "abc:",
            ":abc",
            "fooo;bar",
            "foo bar",
            "ext blah",
            "! ext blah: cmd"
        );
        stream << line << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(line);
        CAPTURE(result.msg.toStdString());

        CHECK(result.line.empty());
        CHECK_FALSE(result.msg.isEmpty());
        CHECK(result.verdict == yaxo::Verdict::error);
    }

    SECTION("condition errors") {
        auto line = GENERATE(
            as<QString>{},
            "foo bar: cmd",  // no such condition: foo
            "ext blah, foo bar: cmd"  // one bad condition
            );

        stream << line << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(line);
        CAPTURE(result.msg.toStdString());

        CHECK(result.line.empty());
        CHECK_FALSE(result.msg.isEmpty());
        CHECK(result.verdict == yaxo::Verdict::error);
    }

    SECTION("common simple condition parsing") {
        auto line = GENERATE(
            as<QString>{},
            "  ext foo  : command --with -opts -- \"$@\""
        );

        stream << line << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(line);
        CAPTURE(result.msg.toStdString());
        CAPTURE(result.line.command.toStdString());

        CHECK(result.line.command == "command --with -opts -- \"$@\"");
        REQUIRE(result.line.conditions.size() == 1);

        CHECK_FALSE(result.line.empty());
        CHECK(result.verdict == yaxo::Verdict::normal);
    }

    SECTION("multiple conditions") {
        auto line = GENERATE(
            as<QString>{},
            " ext foo ,ext   bar , ext  baz: command --with -opts -- \"$@\"",
            "ext foo,ext   bar,ext  baz: command --with -opts -- \"$@\"",
            "ext foo, scheme file, mime .*: command --with -opts -- \"$@\"",
            "ext foo, !scheme file, mime .*: command --with -opts -- \"$@\""
            );

        stream << line << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(line);
        CAPTURE(result.msg.toStdString());
        CAPTURE(result.line.command.toStdString());

        CHECK(result.line.command == "command --with -opts -- \"$@\"");
        REQUIRE(result.line.conditions.size() == 3);

        CHECK_FALSE(result.line.empty());
        CHECK(result.verdict == yaxo::Verdict::normal);

        // TODO: check that these are different conditions for different
        // extensions in specified order.
    }

    SECTION("extension recognition") {
        stream << " ext foo : cmd " << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(result.msg.toStdString());
        CAPTURE(result.line.command.toStdString());

        REQUIRE(result.verdict == yaxo::Verdict::normal);
        REQUIRE(result.line.conditions.size() == 1);

        const auto &cond = result.line.conditions.front();
        REQUIRE(nullptr != dynamic_cast<yaxo::Extension *>(cond.get()));
    }

    SECTION("negated extension recognition") {
        stream << " !ext foo : cmd " << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(result.msg.toStdString());
        CAPTURE(result.line.command.toStdString());

        REQUIRE(result.verdict == yaxo::Verdict::normal);
        REQUIRE(result.line.conditions.size() == 1);

        const auto &cond = result.line.conditions.front();
        auto outer = dynamic_cast<yaxo::Not *>(cond.get());
        REQUIRE(nullptr != outer);
        REQUIRE(nullptr != dynamic_cast<const yaxo::Extension *>(&outer->inner()));
    }

    SECTION("scheme recognition") {
        stream << " scheme foo : cmd " << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(result.msg.toStdString());
        CAPTURE(result.line.command.toStdString());

        REQUIRE(result.verdict == yaxo::Verdict::normal);
        REQUIRE(result.line.conditions.size() == 1);

        const auto &cond = result.line.conditions.front();
        REQUIRE(nullptr != dynamic_cast<yaxo::Scheme *>(cond.get()));
    }

    SECTION("negated scheme recognition") {
        stream << " !scheme foo : cmd " << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(result.msg.toStdString());
        CAPTURE(result.line.command.toStdString());

        REQUIRE(result.verdict == yaxo::Verdict::normal);
        REQUIRE(result.line.conditions.size() == 1);

        const auto &cond = result.line.conditions.front();
        auto outer = dynamic_cast<yaxo::Not *>(cond.get());
        REQUIRE(nullptr != outer);
        REQUIRE(nullptr != dynamic_cast<const yaxo::Scheme *>(&outer->inner()));
    }

    SECTION("mime recognition") {
        stream << " mime foo : cmd " << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(result.msg.toStdString());
        CAPTURE(result.line.command.toStdString());

        REQUIRE(result.verdict == yaxo::Verdict::normal);
        REQUIRE(result.line.conditions.size() == 1);

        const auto &cond = result.line.conditions.front();
        REQUIRE(nullptr != dynamic_cast<yaxo::Mime *>(cond.get()));
    }

    SECTION("negated mime recognition") {
        stream << " !mime foo : cmd " << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(result.msg.toStdString());
        CAPTURE(result.line.command.toStdString());

        REQUIRE(result.verdict == yaxo::Verdict::normal);
        REQUIRE(result.line.conditions.size() == 1);

        const auto &cond = result.line.conditions.front();
        auto outer = dynamic_cast<yaxo::Not *>(cond.get());
        REQUIRE(nullptr != outer);
        REQUIRE(nullptr != dynamic_cast<const yaxo::Mime *>(&outer->inner()));
    }

    SECTION("property check recognition") {
        stream << " if foo : cmd " << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(result.msg.toStdString());
        CAPTURE(result.line.command.toStdString());

        REQUIRE(result.verdict == yaxo::Verdict::normal);
        REQUIRE(result.line.conditions.size() == 1);

        const auto &cond = result.line.conditions.front();
        REQUIRE(nullptr != dynamic_cast<yaxo::PropCheck *>(cond.get()));
    }

    SECTION("negated property check recognition") {
        stream << " !if foo : cmd " << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(result.msg.toStdString());
        CAPTURE(result.line.command.toStdString());

        REQUIRE(result.verdict == yaxo::Verdict::normal);
        REQUIRE(result.line.conditions.size() == 1);

        const auto &cond = result.line.conditions.front();
        auto outer = dynamic_cast<yaxo::Not *>(cond.get());
        REQUIRE(nullptr != outer);
        REQUIRE(nullptr != dynamic_cast<const yaxo::PropCheck *>(&outer->inner()));
    }

    SECTION("env check recognition") {
        stream << " env foo : cmd " << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(result.msg.toStdString());
        CAPTURE(result.line.command.toStdString());

        REQUIRE(result.verdict == yaxo::Verdict::normal);
        REQUIRE(result.line.conditions.size() == 1);

        const auto &cond = result.line.conditions.front();
        REQUIRE(nullptr != dynamic_cast<yaxo::EnvCheck *>(cond.get()));
    }

    SECTION("negated env check recognition") {
        stream << " !env foo : cmd " << "\n";
        auto result = yaxo::nextLine(env, stream);

        CAPTURE(result.msg.toStdString());
        CAPTURE(result.line.command.toStdString());

        REQUIRE(result.verdict == yaxo::Verdict::normal);
        REQUIRE(result.line.conditions.size() == 1);

        const auto &cond = result.line.conditions.front();
        auto outer = dynamic_cast<yaxo::Not *>(cond.get());
        REQUIRE(nullptr != outer);
        REQUIRE(nullptr != dynamic_cast<const yaxo::EnvCheck *>(&outer->inner()));
    }
}
