GCC ?= g++
CLANG ?= clang++-16
PREFIX ?= /usr/local

compile: build
	ninja -C build

install:
	ninja -C build install

check: test-gcc test-clang

test-gcc: build-gcc
	ninja -C build-gcc -j $(shell nproc) test

test-clang: build-clang
	ninja -C build-clang -j $(shell nproc) test

build:
	meson setup build -Dbuildtype=release --prefix "$(PREFIX)"

build-gcc:
	CXX=$(GCC) meson setup build-gcc -Dtests=true -Db_sanitize=address,undefined

build-clang:
	CXX=$(CLANG) meson setup build-clang -Dtests=true -Db_sanitize=address,undefined -Db_lundef=false

clean:
	rm -rf build
	rm -rf build-gcc
	rm -rf build-clang

.PHONY: compile install check test-gcc test-clang
