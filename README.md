# yaxo

*Yet Another xdg-open*.

It's a file opener which replaces a well-known xdg-open script and extends its
configuration capabilities. It was directly inspired by Rifle, a file opener for
Ranger file manager.

## Dependencies

* qt6-base-dev (compile-time core QT6 library)
* meson
* C++ compiler which supports C++14 features.

For runtime dependencies, yaxo requires libqt6core.

## Installation

Yaxo uses meson build system. The following commands will build and install its
Release version in `/usr/local`:

```
$ meson setup build -Dbuildtype=release --prefix "${HOME}/.local"
$ ninja -C build
$ ninja -C build install
```

## Configuration file

Yaxo searches for configuration in e.g. `~/.config/yaxo/yaxorc`. Each line of
`yaxorc` contains a comma-separated list of conditions, followed by a colon,
followed by a command executed when all of conditions are met. Following
conditions are supported:

* `path <regex>`: regular expression matching a full path/URL
* `ext <regex>`: regular expression matching a file extension
* `mime <regex>`: regular expression matching file's mimetype
* `scheme <regex>`: regular expression matching scheme of first passed argument
  (e.g. `file` or `http` or `ftp`, etc.)
* `env <variable>`: the environment variable "variable" is not empty
* `if <property>`: checking various settings of environment settings or passed
  arguments. The following properties can be checked:
  - `terminal`: set to true if yaxo is executed from a terminal
  - `X`: set to true if X server is running (`DISPLAY` environment variable
    is set)
  - `Wayland`: set to true if Wayland is running (`WAYLAND_DISPLAY`
    environment variable is set)
  - `graphical`: set to true if any graphical environment is detected
  - `exists`: set to true if checked argument exists on a file system

Condition can be negated by adding exclemation mark before its name (so for
example `!if terminal` assures that command is not executed inside a
terminal) and `!env FOO` assures that environment variable FOO is not set (or
is empty).

Conditions are checked from top to bottom. First command which matches all
conditions is executed.

Example:

```
ext txt, if terminal:  vim -- "$@"
ext txt, if X:         xterm -e vim -- "$@"

# Different terminal for Wayland and X11
mime text/.*, if terminal:  vim -- "$@"
mime text/.*, if Wayland: alacritty -e vim -- "$@"
mime text/.*, if X: st -e vim -- "$@"

# Images from the internet
scheme https?, path .*\.(png|jpg), if graphical: feh -- "$@"

# Browser.
ext x?html?, if graphical: firefox -- "$@"
ext x?html?, if terminal: w3m -- "$@"

scheme https?, !if graphical: firefox -- "$@"
scheme https?, !if terminal: w3m -- "$@"

# default file manager
mime inode/directory: vifm -- "$@"

# catch-all rules
ext .*, if terminal:  vim -- "$@"
ext .*, !if terminal: st -e vim -- "$@"
```

## Running tests

```
$ meson setup build -Dtests=true
$ ninja -C build test
```
