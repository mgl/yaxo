/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright (C) 2023 Michał Góral.
 */

#include "defs.hpp"
#include "error.hpp"
#include "rcparser.hpp"
#include "arginfo.hpp"
#include "conditions.hpp"
#include "str.hpp"
#include "env.hpp"
#include "executor.hpp"

#include <QCommandLineParser>
#include <QtDebug>

#include <memory>
#include <vector>
#include <algorithm>

namespace {

auto parseArgs(const QCoreApplication &app)
{
    auto parser = std::make_unique<QCommandLineParser>();
    parser->setApplicationDescription(
        _("xdg-open replacement for opening files and URLs based on clear \n"
          "rules gathered in one configuration file."));

    parser->addOptions({
        {
            {"c", "config"},
            _("Path to configuration file."),
            _("config")
        },

        {
            {"q", "query"},
            _("Query and print informations about arguments, then exit.")
        },

        {
            "verbose",
            _("Enable printing additional debug informations.")
        }
    });

    parser->addVersionOption();
    parser->addHelpOption();


    parser->addPositionalArgument(
        _("files"),
        _("Files/URLs to open."),
        "[file | URL]...");

    parser->process(app);
    return parser;
}

auto infos(const QStringList &args)
{
    std::vector<yaxo::ArgInfo> infos;
    infos.reserve(args.size());

    std::transform(args.begin(), args.end(), std::back_inserter(infos),
        [](const auto& arg){ return yaxo::ArgInfo(arg); });

    return infos;
}

void describeAll(
    const std::vector<std::unique_ptr<yaxo::Condition>> &conditions,
    const yaxo::ArgInfo& info)
{
    for (const auto& cond : conditions) {
        debug(QString("  [%1] %2")
                .arg((*cond)(info) ? "PASS" : "FAIL")
                .arg(cond->describe(info)));
    }
}

bool matchAll(
    const std::vector<std::unique_ptr<yaxo::Condition>> &conditions,
    const yaxo::ArgInfo &info)
{
    auto matched = std::all_of(conditions.begin(), conditions.end(),
        [&info](const auto& cond){ return (*cond)(info); });

    if (debugEnabled()) {
        describeAll(conditions, info);
    }

    return matched;
}

bool defaultOpen(
    const std::vector<yaxo::ArgInfo> &infos,
    const QProcessEnvironment &env,
    const yaxo::Executor &ex)
{
    using Conditions = std::vector<std::unique_ptr<yaxo::Condition>>;

    if (env.contains("BROWSER")) {
        Conditions conds;
        conds.push_back(std::make_unique<yaxo::Scheme>("https?"));
        if (matchAll(conds, infos[0]))
            return ex.exec("$BROWSER \"$@\"", infos);
    }

    if (env.contains("VISUAL")) {
        Conditions conds;
        conds.push_back(std::make_unique<yaxo::Mime>("text/.*?"));
        if (matchAll(conds, infos[0]))
            return ex.exec("$VISUAL \"$@\"", infos);
    }

    if (env.contains("EDITOR")) {
        Conditions conds;
        conds.push_back(std::make_unique<yaxo::Mime>("text/.*?"));
        if (matchAll(conds, infos[0]))
            return ex.exec("$EDITOR \"$@\"", infos);
    }

    return false;
}

void printArgInfos(const std::vector<yaxo::ArgInfo> &infos)
{
    auto y = _("yes");
    auto n = _("no");

    for (const auto &info : infos) {
        print(info.arg);
        print(_("  Scheme: %1").arg(info.scheme));
        print(_("  MIME Type: %1").arg(info.mime.name()));
        print(_("  Extension: %1").arg(info.ext));
        print(_("  Exists: %1").arg(info.exists ? y : n));
    }
}

bool openFiles(
    const std::vector<yaxo::ArgInfo> &infos,
    const QProcessEnvironment &env,
    QFile &rc,
    const yaxo::Executor &ex)
{
    auto stream = yaxo::open(rc);
    unsigned line = 0;
    while (true) {
        ++line;
        auto pr = yaxo::nextLine(env, *stream);

        switch (pr.verdict) {
        case yaxo::Verdict::normal:
            debug(_("Checking if '%1' should be run").arg(pr.line.command));
            if (matchAll(pr.line.conditions, infos[0])) {
                return ex.exec(pr.line.command, infos);
            }
            break;

        case yaxo::Verdict::error:
            if (!pr.msg.isEmpty())
                eprint(_("%1:%2: %3").arg(rc.fileName()).arg(line).arg(pr.msg));
            break;

        case yaxo::Verdict::eof:
            return false;

        default:
            break;
        }
    }

    return false;
}

}  // namespace

namespace yaxo {

int do_main(QCoreApplication &app, const Executor &ex)
{
    try {
        auto parser = parseArgs(app);

        enableDebug(parser->isSet("verbose"));

        const auto& args = infos(parser->positionalArguments());
        yaxo::expect_msg(!args.empty(), _("No file/URL to open."));

        if (parser->isSet("query")) {
            printArgInfos(args);
            return 0;
        }

        const auto& env = yaxo::getenv();
        auto rc = yaxo::findRC(parser->value("config"));

        // Interface Executor::exec() has bool interface, but we're using
        // ShellExecutor in production code, which runs execl. It won't return
        // in case of success, so in fact this checking of bool opened is to
        // simulate the behaviour of execl for non-ShellExecutors (tests).
        bool opened = false;
        if (rc)
            opened = openFiles(args, env, *rc, ex);
        if (!opened)
            opened = defaultOpen(args, env, ex);
        if (opened)
            return EXIT_SUCCESS;

        // At this point everything failed and program will return
        // unsuccessfully. Report all errors which can be helpful for users to
        // debug the problem.
        if (!rc)
            eprint(_("Couldn't find yaxorc file."));

        eprint(_("Opening of '%1' failed: no matching rules.")
                .arg(args[0].arg));
    }
    catch (const yaxo::SystemExit &e) {
        return e.err;
    }
    catch (const std::exception &e) {
        eprint(e.what());
        return EXIT_FAILURE;
    }

    // yaxo's correct behaviour is to exec() at one point, so if it exits
    // "normally", it means that id didn't open anything.
    return EXIT_FAILURE;
}

}  // namespace yaxo
