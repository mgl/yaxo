/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright (C) 2023 Michał Góral.
 */

#pragma once

#include <QCoreApplication>

namespace yaxo {

class Executor;

int do_main(QCoreApplication&, const Executor&);


}  // namespace yaxo
