/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright (C) 2023 Michał Góral.
 */

#include <QString>

#include <unistd.h>

#include "executor.hpp"
#include "error.hpp"
#include "str.hpp"
#include "arginfo.hpp"

namespace {

QString quote(QString str)
{
    str.replace("'", "'\\''").prepend("'").append("'");
    return str;
}

QString quote(const std::vector<yaxo::ArgInfo> &infos)
{
    QString ret;
    for (const auto& info : infos) {
        if (!ret.isEmpty())
            ret.append(" ");
        ret.append(quote(info.arg));
    }
    return ret;
}

}  // namespace

namespace yaxo {

bool ShellExecutor::exec(const QString &cmd, const std::vector<yaxo::ArgInfo>& args) const
{
    // "set -- arg1 arg2" - assign arg1 to $1, arg2 to $2 and so on
    auto shc = QString("set -- %1; %2").arg(quote(args)).arg(cmd);
    debug(_("Executing: /bin/sh -c %1").arg(shc));
    auto shc_str = shc.toStdString();
    execl("/bin/sh", "sh", "-c", shc_str.c_str(), nullptr);
    eprint(_("execl(/bin/sh, -c, <cmd>) failed. This shouldn't happen."));
    eprint(_("  note: is /bin/sh available on your system?"));
    throw yaxo::SystemExit();
}

}  // namespace yaxo

