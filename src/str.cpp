/*
 * Copyright (C) 2018 Michał Góral.
 * 
 * This file is part of Yaxo
 * 
 * Yaxo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Yaxo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Yaxo. If not, see <http://www.gnu.org/licenses/>.
 */

#include "str.hpp"

#include <QCoreApplication>
#include <QTextStream>

namespace {

bool DEBUG_ENABLED = false;

}  // namespace 

QString _(const char *sourceText)
{
    return QCoreApplication::translate("translation", sourceText);
}

QString _P(const char *sourceText, int n)
{
    return QCoreApplication::translate("translation", sourceText, nullptr, n);
}

void print(const QString &str)
{
    static QTextStream out(stdout);
    out << str << Qt::endl;
}

void eprint(const QString &str)
{
    static QTextStream err(stderr);
    err << str << Qt::endl;
}

void debug(const QString &str)
{
    if (DEBUG_ENABLED)
        return print(str);
}

void enableDebug(bool enable)
{
    DEBUG_ENABLED = enable;
}

bool debugEnabled()
{
    return DEBUG_ENABLED;
}
