/*
 * Copyright (C) 2018 Michał Góral.
 * 
 * This file is part of Yaxo
 * 
 * Yaxo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Yaxo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Yaxo. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONDITIONS_HPP_
#define CONDITIONS_HPP_

#include <QFileInfo>
#include <QProcessEnvironment>
#include <QHash>

namespace yaxo {

struct ArgInfo;

class Condition
{
public:
    Condition();
    virtual ~Condition() = default;
    bool operator()(const ArgInfo &file) const;
    QString describe(const ArgInfo &file) const;

protected:
    virtual bool check(const ArgInfo &file) const = 0;
    virtual QString describeCheck(const ArgInfo &file) const = 0;
};

class Not : public Condition
{
public:
    explicit Not(std::unique_ptr<Condition> other);
    const Condition& inner() const;

protected:
    bool check(const ArgInfo&) const override final;
    QString describeCheck(const ArgInfo&) const override;

private:
    std::unique_ptr<Condition> cond_;
};

class RegexCondition : public Condition
{
public:
    explicit RegexCondition(const QString &pattern);

protected:
    bool match(const QString &str) const;
    const QString& pattern() const;

private:
    QString pattern_;
};

class Path : public RegexCondition
{
public:
    using RegexCondition::RegexCondition;

protected:
    bool check(const ArgInfo &file) const override;
    QString describeCheck(const ArgInfo &file) const override;
};

class Extension : public RegexCondition
{
public:
    using RegexCondition::RegexCondition;

protected:
    bool check(const ArgInfo &file) const override;
    QString describeCheck(const ArgInfo &file) const override;
};

class Mime : public RegexCondition
{
public:
    using RegexCondition::RegexCondition;

protected:
    bool check(const ArgInfo &file) const override final;
    QString describeCheck(const ArgInfo &file) const override;

private:
    QString pattern_;
};

class Scheme : public RegexCondition
{
public:
    using RegexCondition::RegexCondition;

protected:
    bool check(const ArgInfo &file) const override final;
    QString describeCheck(const ArgInfo &file) const override;

private:
    QString pattern_;
};

class PropCheck : public Condition
{
public:
    explicit PropCheck(const QString &prop, const QProcessEnvironment &env);

protected:
    bool check(const ArgInfo&) const override final;
    QString describeCheck(const ArgInfo&) const override;

private:
    QString prop_;
    const QProcessEnvironment &env_;
};

class EnvCheck : public Condition
{
public:
    explicit EnvCheck(const QString &prop, const QProcessEnvironment &env);

protected:
    bool check(const ArgInfo&) const override final;
    QString describeCheck(const ArgInfo&) const override;

private:
    QString var_;
    const QProcessEnvironment &env_;
};

}  // namespace yaxo

#endif  // CONDITIONS_HPP_
