/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright (C) 2023 Michał Góral.
 */

#pragma once

#include <QString>

namespace yaxo {

struct ArgInfo;

class Executor {
public:
    virtual bool exec(const QString&, const std::vector<yaxo::ArgInfo>&) const = 0;
};

class ShellExecutor : public Executor{
public:
    bool exec(const QString& cmd, const std::vector<yaxo::ArgInfo>& args) const override final;
};
    

}  // namespace yaxo
