/*
 * Copyright (C) 2018 Michał Góral.
 * 
 * This file is part of Yaxo
 * 
 * Yaxo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Yaxo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Yaxo. If not, see <http://www.gnu.org/licenses/>.
 */

#include "arginfo.hpp"

#include <QFileInfo>
#include <QUrl>
#include <QMimeDatabase>

namespace yaxo {

ArgInfo::ArgInfo(const QString &in) : arg(in.trimmed())
{
    auto url = QUrl(arg);
    scheme = url.scheme().isEmpty() ? "file" : url.scheme();

    QMimeDatabase db;
    mime = db.mimeTypeForUrl(url);

    if (scheme == "file") {
        auto file = QFileInfo(arg);
        ext = file.completeSuffix();
        exists = file.exists();
    }
}

}  // namespace yaxo
