/*
 * Copyright (C) 2018 Michał Góral.
 * 
 * This file is part of Yaxo
 * 
 * Yaxo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Yaxo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Yaxo. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ERROR_HPP_
#define ERROR_HPP_

#include <stdexcept>
#include <QString>

namespace yaxo {

class SystemExit : public std::runtime_error
{
public:
    SystemExit();
    explicit SystemExit(int err);

    int err {EXIT_FAILURE};
};

void expect_msg(bool cond, const char * msg);
void expect_msg(bool cond, const QString &msg);

}  // namespace yaxo

#endif  // ERROR_HPP_

