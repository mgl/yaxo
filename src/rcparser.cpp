/*
 * Copyright (C) 2018 Michał Góral.
 * 
 * This file is part of Yaxo
 * 
 * Yaxo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Yaxo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Yaxo. If not, see <http://www.gnu.org/licenses/>.
 */

#include "rcparser.hpp"
#include "defs.hpp"
#include "error.hpp"
#include "str.hpp"

#include <QStandardPaths>
#include <QDir>
#include <QTextStream>

#include <stdexcept>

namespace yaxo {

namespace {

std::pair<QString, QString> splitTwo(const QString &str)
{
    auto simple = str.simplified();
    auto spl = simple.split(' ', Qt::SkipEmptyParts);
    if (spl.size() != 2)
        throw std::invalid_argument("unexpected split size");
    return {spl[0], spl[1]};
}

}  // namespace 

bool RCLine::empty() const
{
    return command.isEmpty() && conditions.empty();
}

std::unique_ptr<QFile> findRC(const QString &path)
{
    const auto cfgloc = QStandardPaths::AppConfigLocation;

    auto rcfile = path.isEmpty() ? QStandardPaths::locate(cfgloc, RC) : path;
    if (!rcfile.isEmpty())
        return std::make_unique<QFile>(rcfile);

    return nullptr;
}

std::unique_ptr<QTextStream> open(QFile &rc)
{
    if (!rc.isOpen() && !rc.open(QIODevice::ReadOnly | QIODevice::Text)) {
        eprint(_("Faied to open %1").arg(qUtf8Printable(rc.fileName())));
        throw yaxo::SystemExit();
    }

    return std::make_unique<QTextStream>(&rc);
}

ParsingResult nextLine(const QProcessEnvironment &env, QTextStream &rcs)
{
    auto result = ParsingResult{};
    result.verdict = Verdict::error;

    auto line = rcs.readLine();
    
    // EOF
    if (line.isNull()) {
        result.verdict = Verdict::eof;
        return result;
    }

    line = line.simplified();

    // comments and empty lines
    if (line.startsWith('#') || line.isEmpty()) {
        result.verdict = Verdict::skip;
        return result;
    }

    // ext txt, mime htm.?: browser -- "$@"
    auto condsstr = line.section(':', 0, 0);
    auto cmdstr = line.section(':', 1, 1).trimmed();

    if (condsstr.isEmpty()) {
        result.msg = _("missing condsstr");
        return result;
    }
    if (cmdstr.isEmpty()) {
        result.msg = _("missing command");
        return result;
    }

    std::vector<std::unique_ptr<Condition>> conditions;
    for (auto str : condsstr.split(',', Qt::SkipEmptyParts)) {
        QString key, value;
        try {
            std::tie(key, value) = splitTwo(str);
        }
        catch (const std::invalid_argument&) {
            result.msg = _("invalid condition: %1").arg(str);
            return result;
        }

        bool negate = key.startsWith('!');
        if (negate) {
            key.remove(0, 1);
        }

        std::unique_ptr<Condition> cond;
        if (key == "path") {
            cond = std::make_unique<Path>(value);
        }
        else if (key == "ext") {
            cond = std::make_unique<Extension>(value);
        }
        else if (key == "mime") {
            cond = std::make_unique<Mime>(value);
        }
        else if (key == "scheme") {
            cond = std::make_unique<Scheme>(value);
        }
        else if (key == "env") {
            cond = std::make_unique<EnvCheck>(value, env);
        }
        else if (key == "if") {
            cond = std::make_unique<PropCheck>(value, env);
        }
        else {
            result.msg = _("incorrect condition type: %1").arg(key);
            return result;
        }

        conditions.push_back(
            negate
            ? std::make_unique<Not>(std::move(cond))
            : std::move(cond));
    }

    using std::swap;
    swap(result.line.conditions, conditions);
    swap(result.line.command, cmdstr);
    result.verdict = Verdict::normal;

    return result;
}

}  // namespace yaxo
