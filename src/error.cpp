/*
 * Copyright (C) 2018 Michał Góral.
 * 
 * This file is part of Yaxo
 * 
 * Yaxo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Yaxo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Yaxo. If not, see <http://www.gnu.org/licenses/>.
 */

#include "error.hpp"
#include "str.hpp"

#include <QtDebug>

namespace yaxo {

SystemExit::SystemExit() :
    std::runtime_error(std::to_string(EXIT_FAILURE))
{
}

SystemExit::SystemExit(int err) :
    std::runtime_error(std::to_string(err)),
    err(err)
{
}

void expect_msg(bool cond, const char * msg)
{
    if (!cond) {
        eprint(msg);
        throw SystemExit();
    }
}

void expect_msg(bool cond, const QString &msg)
{
    if (!cond) {
        eprint(msg);
        throw SystemExit();
    }
}

}  // namespace yaxo
