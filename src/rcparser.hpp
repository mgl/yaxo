/*
 * Copyright (C) 2018 Michał Góral.
 * 
 * This file is part of Yaxo
 * 
 * Yaxo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Yaxo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Yaxo. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RCPARSER_HPP_
#define RCPARSER_HPP_

#include "conditions.hpp"

#include <QFile>
#include <QString>
#include <QFileInfo>
#include <QVector>
#include <QTextStream>
#include <QProcessEnvironment>

#include <memory>
#include <vector>

namespace yaxo {

enum class Verdict
{
    eof = 1,
    skip,
    normal,
    error,
};

struct RCLine
{
    std::vector<std::unique_ptr<Condition>> conditions;
    QString command;

    bool empty() const;
};

struct ParsingResult
{
    RCLine line;
    QString msg;
    Verdict verdict;
};

std::unique_ptr<QFile> findRC(const QString &path);
std::unique_ptr<QTextStream> open(QFile &rc);
ParsingResult nextLine(const QProcessEnvironment &env, QTextStream &rcs);

}  // namespace yaxo

#endif  // RCPARSER_HPP_
