/*
 * Copyright (C) 2018 Michał Góral.
 * 
 * This file is part of Yaxo
 * 
 * Yaxo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Yaxo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Yaxo. If not, see <http://www.gnu.org/licenses/>.
 */

#include "conditions.hpp"
#include "arginfo.hpp"
#include "str.hpp"
#include "error.hpp"

#include <QString>
#include <QStringList>
#include <QRegularExpression>
#include <QProcessEnvironment>

#include <unistd.h>

namespace yaxo {

namespace  {

bool hasTerm(const ArgInfo&, const QProcessEnvironment&)
{
    return (ttyname(0) != nullptr &&
            ttyname(1) != nullptr &&
            ttyname(2) != nullptr);
}

bool hasDisplay(const ArgInfo&, const QProcessEnvironment &env)
{
    return env.contains("DISPLAY");
}

bool hasWayland(const ArgInfo&, const QProcessEnvironment &env)
{
    return env.contains("WAYLAND_DISPLAY");
}

bool hasGraphicalEnv(const ArgInfo& file, const QProcessEnvironment &env)
{
    return hasDisplay(file, env) || hasWayland(file, env);
}

bool exists(const ArgInfo &file, const QProcessEnvironment&)
{
    return file.exists;
}

// a map of check functions used by PropCheck
using CheckFn = bool (*)(const ArgInfo&, const QProcessEnvironment&);
const QHash<QString, CheckFn> PROP_CHECKERS = {
    {"terminal", &hasTerm},
    {"X", &hasDisplay},
    {"Wayland", &hasWayland},
    {"graphical", &hasGraphicalEnv},
    {"exists", &exists},
};


}  // namespace

// Condition Interface
/////////////////////////////////////////////////////////////

Condition::Condition()
{
}

bool Condition::operator()(const ArgInfo &file) const
{
    return check(file);
}

QString Condition::describe(const ArgInfo &file) const
{
    return describeCheck(file);
}

// Not
/////////////////////////////////////////////////////////////

Not::Not(std::unique_ptr<Condition> cond) : cond_(std::move(cond))
{
    expect_msg(static_cast<bool>(cond_), "uninitialized condition passed to Not");
}

const Condition& Not::inner() const {
    return *cond_;
}

bool Not::check(const ArgInfo &file) const
{
    return !(*cond_)(file);
}

QString Not::describeCheck(const ArgInfo &file) const
{
    return _("(negated) %1")
        .arg(cond_->describe(file));
}

// Common code for regular expressions
/////////////////////////////////////////////////////////////

RegexCondition::RegexCondition(const QString &pattern) :
    pattern_(pattern)
{
}

bool RegexCondition::match(const QString &str) const
{
    // matches the whole string, like: ^foo$
    auto re = QRegularExpression("\\A(?:" + pattern_ + ")\\z");
    if (!re.isValid()) {
        auto err = re.errorString();
        eprint(_("Incorrect regular expression: '%1'").arg(pattern_));
        eprint(_("  note: %1").arg(qUtf8Printable(err)));
        return false;
    }

    auto m = re.match(str);
    return m.hasMatch();
}

const QString& RegexCondition::pattern() const
{
    return pattern_;
}

// Path
/////////////////////////////////////////////////////////////

bool Path::check(const ArgInfo &file) const
{
    return match(file.arg);
}

QString Path::describeCheck(const ArgInfo &file) const
{
    return _("path: '%1' matches pattern '%2'")
        .arg(file.arg)
        .arg(pattern());
}

// Extension
/////////////////////////////////////////////////////////////

bool Extension::check(const ArgInfo &file) const
{
    return match(file.ext);
}

QString Extension::describeCheck(const ArgInfo &file) const
{
    return _("ext: '%1' has an extension '%2' matching pattern %3")
        .arg(file.arg)
        .arg(file.ext)
        .arg(pattern());
}

// Mime
/////////////////////////////////////////////////////////////

bool Mime::check(const ArgInfo &file) const
{
   return match(file.mime.name());
}

QString Mime::describeCheck(const ArgInfo &file) const
{
    return _("mime: %1 matches pattern %2")
        .arg(file.mime.name())
        .arg(pattern());
}

// Scheme
/////////////////////////////////////////////////////////////

bool Scheme::check(const ArgInfo &file) const
{
    return match(file.scheme);
}

QString Scheme::describeCheck(const ArgInfo &file) const
{
    return _("scheme: %1 matches pattern %2")
        .arg(file.scheme)
        .arg(pattern());
}

// PropCheck
/////////////////////////////////////////////////////////////

PropCheck::PropCheck(const QString &prop, const QProcessEnvironment &env) :
    prop_(prop),
    env_(env)
{
}

bool PropCheck::check(const ArgInfo &file) const
{
    auto found = PROP_CHECKERS.find(prop_.toStdString().c_str());
    if (found != PROP_CHECKERS.end()) {
        return found.value()(file, env_);
    }

    QStringList acc(PROP_CHECKERS.keys());

    eprint(_("Unknown property: '%1'").arg(prop_));
    eprint(_("  note: expected one of: '%1'").arg(acc.join(", ")));
    return false;
}

QString PropCheck::describeCheck(const ArgInfo &) const
{
    return _("if: %1")
        .arg(prop_);
}

// EnvCheck
/////////////////////////////////////////////////////////////

EnvCheck::EnvCheck(const QString &var, const QProcessEnvironment &env) :
    var_(var),
    env_(env)
{
}

bool EnvCheck::check(const ArgInfo &) const
{
    return env_.value(var_) != "";
}

QString EnvCheck::describeCheck(const ArgInfo &) const
{
    return _("env: %1 is set")
        .arg(var_);
}

}  // namespace yaxo
