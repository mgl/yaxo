if exists("b:current_syntax")
    finish
endif

syn match yaxoComment /#.*$/

syn region yaxoConditionList start=/^[^#]/ end=/:/ oneline
syn region yaxoCommand start=/:/ end=/$/ oneline

syn keyword yaxoConditional path ext scheme containedin=yaxoConditionList nextgroup=yaxoConditionBodyStr
syn match yaxoConditionBodyStr /[^,:]\+/ contained containedin=yaxoConditionList skipwhite

syn keyword yaxoConditional mime containedin=yaxoConditionList nextgroup=yaxoConditionBodyMime
syn match yaxoConditionBodyMime /[^,:]\+/ contained containedin=yaxoConditionList skipwhite

syn keyword yaxoConditional if containedin=yaxoConditionList nextgroup=yaxoConditionBodyIf
syn keyword yaxoConditionBodyIf terminal X exists containedin=yaxoConditionList skipwhite

syn match yaxoSep /:/ contained containedin=yaxoCommand nextgroup=yaxoProgName skipwhite

hi def link yaxoComment Comment
hi def link yaxoConditional Conditional
hi def link yaxoConditionBodyStr String
hi def link yaxoConditionBodyIf Constant
hi def link yaxoConditionBodyMime Typedef
hi def link yaxoSep Delimiter

let b:current_suntax = "yaxorc"
